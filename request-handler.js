let url = require('url');
let log = require('@mikwal/node-console-logger').createLogger('request-handler');

module.exports = requestHandler;

function requestHandler(callback) {
    
    return (request, response) => {

        let sendResponse = (status, body, headers) => {
            response.writeHead(status || 200, { 'Content-Type': 'text/plain', ...(headers || {}) });
            response.end(body || '');
        };

        let { method, headers } = request;
        let { pathname } = url.parse(request.url);

        if (method === 'POST') {
            let tStart = Date.now();
            let data = [];
            request.on('data', chunk => data.push(chunk));
            request.on('error', error => log.warning('Error reading request body', error));
            request.on('end', () => Promise.
                resolve({ url: pathname, body: Buffer.concat(data), headers }).
                then(callback).
                then(
                    ({ status, body, headers }) => {
                        sendResponse(status, body, headers);
                        log.info(`Request '${method} ${pathname}' finished (${status}) in ${Date.now() - tStart}ms`);
                    },
                    error => {
                        sendResponse(500, error.toString());
                        log.info(`Request failed for '${method} ${pathname}'`, error);
                    }
                ));
            return;
        }

        sendResponse(400, 'Bad Request');
    };
}