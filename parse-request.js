module.exports = parseRequest;

let requestSchema = {
    type: 'object',
    required: ['html'],
    properties: {
        html: { type: 'string' },
        options: {
            type: 'object',
            properties: {
                displayHeaderFooter: { type: 'boolean' },
                headerTemplate: { type: 'string' },
                footerTemplate: { type: 'string' },
                landscape: { type: 'boolean' },
                format: {
                    type: 'string',
                    enum: ['Letter', 'Legal', 'Tabload', 'Ledger', 'A0', 'A1', 'A2', 'A3', 'A4', 'A5']
                },
                width: { type: 'string' },
                height: { type: 'string' },
                scale: { type: 'number' },
                margin: {
                    type: 'object',
                    properties: {
                        top: { type: 'string' },
                        right: { type: 'string' },
                        bottom: { type: 'string' },
                        left: { type: 'string' }
                    }
                }
            }
        }
    }
};

function parseRequest(request) {
    request = JSON.parse(request.toString('utf8'));
    request = validate(request, requestSchema);
    return request;
}

function validate(input, schema, path = '$') {
    let type = typeof input;

    if (type === 'undefined' || !schema) {
        return undefined;
    }

    if (type !== schema.type) {
        throw new Error(`Invalid type '${type}' for property '${path}', expected '${schema.type}'`);
    }

    if (type === 'string' && schema.enum && schema.enum.indexOf(input) < 0) {
        throw new Error(`Invalid value '${input}' for property '${path}', expected one of '${schema.enum.join(', ')}'`);
    }

    if (type === 'object') {
        let output = {};

        if (Array.isArray(schema.required)) {
            for (let key of schema.required) {
                if (typeof input[key] === 'undefined') {
                    throw new Error(`Property '${key}' is required for object '${path}'`);
                }
            }
        }

        for (let key of Object.keys(input)) {
            let value = validate(input[key], schema.properties[key], `${path}.${key}`);
            if (typeof value !== 'undefined') {
                output[key] = value;
            }
        }

        return output;
    }

    return input;
}