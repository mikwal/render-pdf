FROM node:10-slim

# Allow node to listen on ports < 1024
RUN apt-get update && apt-get install -y libcap2-bin \
    && setcap CAP_NET_BIND_SERVICE=+eip /usr/local/bin/node

# See https://crbug.com/795759
RUN apt-get update && apt-get install -yq libgconf-2-4

# https://github.com/GoogleChrome/puppeteer/blob/master/docs/troubleshooting.md#running-puppeteer-in-docker
RUN apt-get update && apt-get install -y wget --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get purge --auto-remove -y curl \
    && rm -rf /src/*.deb

RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
    && mkdir -p /home/pptruser/Downloads \
    && chown -R pptruser:pptruser /home/pptruser \
    && mkdir -p /app/render-pdf \
    && chown -R pptruser:pptruser /app

USER pptruser

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true" \
    PUPPETEER_EXECUTABLE_PATH="google-chrome-unstable" \
    PUPPETEER_ARGS="--disable-dev-shm-usage --no-sandbox" \
    NODE_ENV="production"

WORKDIR /app/render-pdf
COPY package.json yarn.lock ./
RUN yarn install --production
COPY *.js ./

EXPOSE 80
ENTRYPOINT ["node", "main.js"]