# render-pdf

Micro service for rendering HTML to PDF using puppeteer

## Usage

It listens on port `80` for `POST /` requests containing a JSON or HTML payload, change default port by setting environment variable `PORT`.  
