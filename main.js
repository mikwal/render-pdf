let { runHttpServer } = require('@mikwal/node-http-server');
let requestHandler = require('./request-handler');
let parseRequest = require('./parse-request');
let render = require('./render');

runHttpServer(requestHandler(async ({ headers, body }) => {
    
    let contentType = headers['content-type'];
    let html, options;

    switch (contentType) {
        
        case 'text/html':
            html = body;
            break;
        
        case 'application/json':
            try {
                body = parseRequest(body);
            }
            catch (error) {
                return {
                    status: 400,
                    body: `Bad Request (could not parse request body: ${error.message})`
                }
            }
            html = Buffer.from(body.html);
            options = body.options;
            break;
        
        default:
            return {
                status: 400,
                body: `Bad Request (invalid content type '${contentType}')`
            };
    }

    let pdf = await render(html, options);

    return {
        status: 200,
        headers: { 'Content-Type': 'application/pdf' },
        body: pdf
    }
}));