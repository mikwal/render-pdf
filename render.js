let puppeteer = require('puppeteer');
let log = require('@mikwal/node-console-logger').createLogger('render');

module.exports = render;

let defaultOptions = {
    format: 'A4',
    printBackground: true
};

let fixedOptions = {
    path: null
}

async function render(html, options) {

    let browser = await puppeteer.launch({
        executablePath: process.env['PUPPETEER_EXECUTABLE_PATH'],
        args: (s => s && s.split(/\s+/g))(process.env['PUPPETEER_ARGS']),
        ignoreDefaultArgs: ['--disable-gpu']
    });

    let page = await browser.newPage();

    if (!/^\s*(true|enabled|1)\s*$/g.test(process.env['UNSAFE_RENDER_MODE'])) {
        page.setJavaScriptEnabled(false);
        page.setRequestInterception(true);
        page.on('request', request => {
            const url = request.url();
            if (!(url.startsWith('data:') || isAllowedExternalRequest(url))) {
                log.warning(`External request blocked: ${request.url()}`);
                request.respond({
                    status: 404,
                    contentType: 'text/plain',
                    body: 'Not Found'
                });
                return;
            }
            request.continue();
        });
    }

    await page.goto(`data:text/html;charset=utf-8;base64,${html.toString('base64')}`, { waitUntil: 'networkidle0' });

    let pdf = await page.pdf({
        ...defaultOptions,
        ...options,
        ...fixedOptions
    });

    await browser.close();

    return pdf;
};

function isAllowedExternalRequest(url) {
    return (process.env.ALLOWED_EXTERNAL_RESOURCES || '').
        split(';').
        map(prefix => prefix.trim()).
        filter(prefix => prefix.length > 0).
        some(prefix => url.startsWith(prefix))
}